import youtube_dl
import logging

from discord.ext import commands
from .guildplayer import GuildPlayer, GuildPlayerMode
from .yt_loader import YTLoader


class MusicPlayerPlugin(commands.Cog):
    def __init__(self, bot, localConfig):
        self.bot            = bot
        self.loader         = YTLoader()
        self.musicPlayer    = None
        self.localConfig    = localConfig
        self.channelForMsg  = self.localConfig.get('MUSICBOT', 'channelForMsg', fallback='')
        self.channelForBot  = self.localConfig.get('MUSICBOT', 'channelForBot', fallback='')

    # Check if the channels are correct
    async def onlySpecifiedChannel(self, ctx):
        # Deklaration
        author          = ctx.author

        # Check if Correct Message Channel
        if ctx.message.channel.name != self.channelForMsg:
            await ctx.send('Play wishes only allowed in Channel: ' + self.channelForMsg)
            return False
        else:
            # Delete Play Msg
            await self.clearMsgChannel(ctx, 1)

        # Check if Correct Voice Channel
        if not str(author.voice.channel) == str(self.channelForBot):
            await ctx.send('You must be in the voice channel: ' + self.channelForBot)
            return False

        return True

    # Disconnect the bot if there's no one to listen
    @commands.Cog.listener()
    async def on_voice_state_update(self, member, before, after):
        # When only Micro/Audio enable/disable
        if before.channel is after.channel:
            return

        # Get the Bot
        musicBot = self.createMusicPlayer(member.guild)

        # When Bot Disconnected or Channel is uninvolved
        if not musicBot.is_connected or before.channel != musicBot.channel:
            return

        # Get Bots and Member Keys
        allMembers = musicBot.channel.members
        memberIds = musicBot.channel.voice_states.keys()

        # The Bot must be in the Channel
        if allMembers is None:
            return

        # Convert to list
        memberIds = list(memberIds)

        # Remove Bots from list
        for channelMember in allMembers:
            memberID = channelMember.id
            if channelMember.bot:
                memberIds.remove(memberID)

        # Disconnect if no one in Channel
        if memberIds is None or len(memberIds) == 0:
            await musicBot.disconnect()

    # Retrieves or creates a GuildPlayer
    def createMusicPlayer(self, guild):
        if self.musicPlayer is not None:
            return self.musicPlayer
        else:
            # Get Config values
            volume              = self.localConfig.get('MUSICBOT', 'default_volume', fallback=100)
            inactivity_timeout  = self.localConfig.get('MUSICBOT', 'connection_timeout', fallback=7200)

            self.musicPlayer = GuildPlayer(
                guild,
                volume=int(volume),
                inactivity_timeout=int(inactivity_timeout))
            return self.musicPlayer

    @commands.Cog.listener()
    async def on_command_error(self, ctx, ignore):
        # Check if the channels are correct
        if not await self.onlySpecifiedChannel(ctx):
            return

        message = '```'
        message += '!mode               -> Switch Mode [SINGLE | LINEAR] Single=only 1 Song, LINEAR=Request List\n'
        message += '!clear [count]      -> Clear the Message Channel\n'
        message += '!play [URL] [lool]  -> Play one Song, can loop it\n'
        message += '!playlist [URL] [loop] [shuffel] \n' \
                   '-> Play Playlist, can loop it after end and shuffel it\n'
        message += '!skip               -> Skip current Song\n'
        message += '!pause              -> Pause current Song\n'
        message += '!stop               -> Stop the Player\n'
        message += '!resume             -> Resume current Song\n'
        message += '!list               -> Show the Request List\n'
        message += '!clearlist          -> Clear the Request List\n'
        message += '```'
        await ctx.send(message)


    @commands.command(name='mode')
    async def modeCMD(self, ctx, mode=None):
        """Clear the Music-Bot Message Channel"""
        # Check if the channels are correct
        if not await self.onlySpecifiedChannel(ctx):
            return

        # Get MusicBot
        musicBot = self.createMusicPlayer(ctx.guild)
        if any(x for x in GuildPlayerMode if x.name == mode):
            musicBot.mode = mode
        elif mode is None:
            await ctx.send("Mode is: " + str(musicBot.mode))
        else:
            await ctx.send("As mode only: SINGLE | LINEAR")

    @commands.command(name='clear', aliases=['c'])
    async def clearCMD(self, ctx, count: int = None):
        """Clear the Music-Bot Message Channel"""
        # Check if the channels are correct
        if not await self.onlySpecifiedChannel(ctx):
            return

        await self.clearMsgChannel(ctx, None if count is None else count+1)

    async def clearMsgChannel(self, ctx, count=None):
        """Clear the Music-Bot Message Channel"""
        # Get Msg Channel
        channel = ctx.channel

        if count is None:
            loopCount = 0
            while loopCount < 5 and len(await channel.history().flatten()) > 0:
                loopCount += 1
                try:
                    await channel.purge()
                except:
                    pass
        else:
            try:
                await channel.purge(limit=count)
            except:
                pass

    @commands.command(name='play', aliases=['p'])
    async def playCMD(self, ctx, url: str, *args):
        """Plays audio. Use "help play" for more info.
        Add "loop" to loop the requested audio. Use "next" or "stop" to cancel it.
        """

        # If loop set
        args = [a.lower() for a in args]
        loop = 'loop' in args

        await self.playMusic(ctx, url, self.loader.loadSong, loop=loop)

    @commands.command(name='playlist')
    async def playlistCMD(self, ctx, url, *args):
        """Adds a youtube playlist to a queue from a url or a tag name. Loop and shuffle as optional arguments."""

        # If shuffel set
        args    = [a.lower() for a in args]
        shuffle = 'shuffle' in args
        loop    = 'loop' in args

        await self.playMusic(ctx, url, self.loader.loadPlaylist, loop=loop, shuffel=shuffle)

    async def playMusic(self, ctx, url, function, loop=False, shuffel=False):
        """Play the the Songs"""
        # Check if the channels are correct
        if not await self.onlySpecifiedChannel(ctx):
            return

        try:
            if url:
                # Get MusicBot
                musicBot = self.createMusicPlayer(ctx.guild)

                # Try to load a tag, if it fails it passes through
                logging.info("Playing {}".format(url))

                # Get all Songs
                songs = await function(url)

                if len(songs) == 1:
                    musicBot.request_song(songs[0], ctx.author, ctx.channel, loop=loop)
                else:
                    if shuffel:
                        musicBot.shuffelList()

                    # Add the Requests
                    musicBot.clearlist()
                    musicBot.mode = GuildPlayerMode.LINEAR

                    for song in songs:
                        musicBot.request_song(song, ctx.author, ctx.channel, loop=loop)
            else:
                await ctx.send("There is nothing to play.")
                return

            await musicBot.connect(ctx.author.voice.channel)
            musicBot.play()

        except youtube_dl.utils.DownloadError as ex:
            message = 'Failed to download video: ' + str(ex)
            logging.error(message)
            await ctx.send(message)

        except Exception as ex:
            logging.error('Error while trying to connect or play audio')
            logging.exception(ex)

            await ctx.send("Error while trying to connect or play audio")

    @commands.command(name='skip', aliases=['next'])
    async def nextCMD(self, ctx):
        """Skips the current song"""
        if not await self.onlySpecifiedChannel(ctx):
            return
        self.createMusicPlayer(ctx.guild).skip()

    @commands.command(name='stop')
    async def stopCMD(self, ctx):
        """Pause the MusicPlayer"""
        if not await self.onlySpecifiedChannel(ctx):
            return
        self.createMusicPlayer(ctx.guild).stop()

    @commands.command(name='pause')
    async def pauseCMD(self, ctx):
        """Stops the MusicPlayer"""
        if not await self.onlySpecifiedChannel(ctx):
            return
        self.createMusicPlayer(ctx.guild).pause()

    @commands.command(name='resume', aliases=['start'])
    async def resumeCMD(self, ctx):
        """Stops the MusicPlayer"""
        if not await self.onlySpecifiedChannel(ctx):
            return
        self.createMusicPlayer(ctx.guild).resume()

    @commands.command(name='clearlist')
    async def clearlistCMD(self, ctx):
        """Clear the MusicPlayer RequestList"""
        if not await self.onlySpecifiedChannel(ctx):
            return
        self.createMusicPlayer(ctx.guild).clearlist()

    @commands.command(name='list')
    async def listCMD(self, ctx):
        """Lists all Song in PlayerList"""
        if not await self.onlySpecifiedChannel(ctx):
            return

        musicBot    = self.createMusicPlayer(ctx.guild)
        count       = len(musicBot)

        # If list empty
        if not count:
            await ctx.send('The playlist is empty')
            return

        # Build the message
        message = f'In the list are [{count}] songs.'
        if count > 20:
            message += ' These are the first 10.\n'

        # Append the titles to message
        titles  = [req.title for req in musicBot]
        titles  = titles[0:10]

        message += '```'
        for idx in range(len(titles)):
            message += str(idx+1) + ". - " + titles[idx] + "\n"
        message += '```'

        await ctx.send(message)
