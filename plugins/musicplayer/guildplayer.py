import asyncio
import discord
import traceback
from .songrequestlist import SongRequestList
from .song import Song

import logging
from enum import Enum


class SongRequest:
    """Represents a song request from a user from a channel
    
    This is used internally by the GuildPlayer to track the songs being played.
    """

    def __init__(self, song, request_user, request_channel, *, loop=False):
        self.song = song
        self.request_user = request_user
        self.request_channel = request_channel
        self.loop = loop

    @property
    def title(self):
        return self.song.title

    @property
    def url(self):
        return self.song.url

    @property
    def source(self):
        return self.song.source


class GuildPlayerMode(Enum):
    SINGLE = "Single"
    LINEAR = "Linear"

    def __str__(self):
        return self.value


class GuildPlayer:
    """A class used to play audio for a guild and manage state.

    This class manages and wraps over discord's VoiceClient.
    Allows iteration over loaded requests"""

    def __init__(self, guild, volume: int = 100, inactivity_timeout=600):
        """Creates a new GuildPlayer"""

        self.guild = guild  # todo: use to validate connections?
        self.requests = SongRequestList()
        self.inactivity_timeout_length = inactivity_timeout
        self.inactivity_timeout = None

        self.connect_lock = asyncio.Lock()
        self.stop_signal = asyncio.Event()
        self.skip_signal = asyncio.Event()

        self.voiceClient = None
        self.isPlaying = False
        self.__volume = volume
        self.__mode = GuildPlayerMode.SINGLE

    def __len__(self):
        return len(self.requests)

    def __iter__(self):
        return self.requests.__iter__()

    @property
    def volume(self):
        """Returns the guild's set volume level"""
        return self.__volume

    @volume.setter
    def volume(self, value: int):
        """Sets the guild's default volume level, and any currently playing music"""
        self.__volume = max(0, min(150, int(value)))
        if self.voiceClient:
            self.voiceClient.source.volume = self.__volume / 100

    @property
    def mode(self):
        """Returns the current GuildPlayerMode setting"""
        return self.__mode

    @mode.setter
    def mode(self, newmode):
        """Sets the current GuildPlayerMode setting"""
        self.__mode = newmode

    @property
    def is_connected(self):
        """Returns the current Connections state, from the GuildPlayer"""
        return self.voiceClient and self.voiceClient.is_connected()

    @property
    def channel(self):
        """Returns the currently connected voice channel if connected, otherwise returns None"""
        if self.voiceClient:
            return self.voiceClient.channel
        return None

    async def connect(self, voiceChannel: discord.VoiceChannel):
        """This is a coroutine. Connect to the voice channel,
        or move to it if already connected to another one.
        Does nothing if already connected to the channel.

        If the player was disconnected and connects, the mode will switch to SINGLE."""

        # Are the Bot already connected, to the right channel -> return
        if self.is_connected and self.voiceClient.channel is voiceChannel:
            return

        # Are the Bot already connected, to the wrong channel -> move to new channel
        if self.is_connected:
            if isinstance(self.voiceClient, discord.VoiceClient):
                await self.voiceClient.move_to(voiceChannel)
        else:
            # Connect
            async with self.connect_lock:
                self.voiceClient = await voiceChannel.connect()

    async def disconnect(self):
        self.stop()
        with await self.connect_lock:
            if self.voiceClient:
                await self.voiceClient.disconnect()
            self.voiceClient = None

    def request_song(self, song: Song, request_user, request_channel, loop=False):
        """Requests a song. The mode decides what happens.
        
        SINGLE: Stops the song, clears the list
        LINEAR: Adds the song to the list

        No matter the mode, the guild player will begin playing if its currently
        not playing. The procedure is added to the asyncio event loop.
        """
        request = SongRequest(song, request_user, request_channel, loop=loop)

        if self.__mode is GuildPlayerMode.SINGLE:
            self.requests.clear()
            self.requests.add(request)
            self.skip()
        else:
            self.requests.add(request)

    def play(self):
        """Tells the GuildPlayer to begin playing. 
        Not a coroutine, but it adds to the asyncio event loop"""
        asyncio.ensure_future(self.__start_playing())

    def skip(self):
        """Tells the player to stop playing the current song and play the next"""
        self.skip_signal.set()
        if self.voiceClient:
            self.voiceClient.stop()

    def pause(self):
        """Tells the player to pause playing entirely. This does not clear the list"""
        self.stop_signal.set()
        if self.voiceClient:
            self.voiceClient.pause()

    def stop(self):
        """Tells the player to stop playing entirely. This does clear the list"""
        self.stop_signal.set()
        if self.voiceClient:
            self.voiceClient.stop()

    def resume(self):
        """Tells the player to resume playing."""
        self.stop_signal.clear()
        if self.voiceClient:
            self.voiceClient.resume()

    def clearlist(self):
        """Clears the request list. Stops playback"""
        self.stop()
        self.requests.clear()

    def shuffelList(self):
        """Shuffel the request list"""
        self.requests.shuffleQueue()

    async def __start_playing(self):
        # If Playing -> return
        if self.isPlaying:
            return

        # Set Playing & Clear Signals
        self.isPlaying = True
        self.stop_signal.clear()
        self.skip_signal.clear()

        # Start Playing
        while self.is_connected and not self.stop_signal.is_set():
            # Get next Song or breakup
            song = self.requests.next()
            if not song:
                break

            # Try Playing
            try:
                # Extra for Footer
                extra = " | Loop" if song.loop else ""

                # Notify for the Request Channel
                playing = discord.Embed()
                playing.title       = f'Now playing {song.title}'
                playing.description = f"Requested by {song.request_user.name}"
                playing.url         = song.url
                playing.set_footer(text=f"Mode: {str(self.mode)} | Volume: {self.__volume}{extra}")
                await song.request_channel.send(embed=playing)

                # Play the song
                if song.loop:
                    self.requests.loop = True
                await self.__play_song(song)

            except:
                traceback.print_exc()
                msg_text = "I couldn't play {}".format(song.title)
                await song.request_channel.send(msg_text)

        self.isPlaying = False

        # Start the disconnect from voice channel timeout
        timeout_coro = self.__wait_timeout(self.inactivity_timeout_length)
        self.inactivity_timeout = asyncio.ensure_future(timeout_coro)

    async def __play_song(self, song):
        """This is a coroutine. Plays the contents of the song over audio.
        Will reset the timeout, and start a timeout after the song completes"""

        # If there is a player_timeout, cancel. If its already cancelled it has no effect
        if self.inactivity_timeout:
            self.inactivity_timeout.cancel()

        # Make sure the client doesn't disconnect in the middle of this by locking
        async with self.connect_lock:
            stop_event  = asyncio.Event()
            loop        = asyncio.get_event_loop()

            def after(error):
                if error:
                    logging.error(error)

                def clear():
                    stop_event.set()

                loop.call_soon_threadsafe(clear)

            ffmpegOpts = {
                'before_options': '-reconnect 1 -reconnect_streamed 1 -reconnect_delay_max 5',
                'options': f'-vn'
            }

            # Get Timestamp if exist
            if '?t=' in song.url:
                timestamp = song.url.split('?t=')[1]
                ffmpegOpts['options'] = f'-vn -ss {timestamp}'
            else:
                ffmpegOpts['options'] = f'-vn'

            source = discord.FFmpegPCMAudio(song.source, **ffmpegOpts)
            source = discord.PCMVolumeTransformer(source)
            source.volume = self.__volume / 100

            self.voiceClient.play(source, after=after)

            # wait until the player is done (triggered by 'after')
            await stop_event.wait()

    async def __wait_timeout(self, length):
        """This is a coroutine. Starts the timeout for the player to disconnect."""
        await asyncio.sleep(length)
        await self.disconnect()
        print('disconnected due to timeout')
