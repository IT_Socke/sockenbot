class Song:
    """Song Data Object"""
    def __init__(self, title: str, url, source):
        self.title  = title
        self.url    = url
        self.source = source
