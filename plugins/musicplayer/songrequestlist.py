import random


class SongRequestList:
    """Represents a queue of songs that can be manipulated"""

    def __init__(self):
        self.shuffle    = False
        self.loop       = False
        self.index      = -1
        self.songs      = []

    def __len__(self):
        return len(self.songs)

    def __iter__(self):
        return self.songs.__iter__()

    def add(self, song):
        """Adds a single song request to the list"""
        self.songs.append(song)

    def extend(self, songs):
        """Adds multiple song requests to the list"""
        self.songs.extend(songs)

    def clear(self):
        """Clear the requests list"""
        self.songs.clear()
        self.index = -1

    def shuffleQueue(self):
        """Randomizes the upcoming songs and enables shuffling"""
        self.shuffle = True
        random.shuffle(self.songs)

    def reset(self, songs, *, loop=False, shuffle=False):
        """Reset the hole request list"""
        # Re-Init and Clear songs
        self.loop = loop
        self.clear()
        self.extend(songs)

        # Shuffle songs if it set
        if shuffle:
            self.shuffleQueue()

    def next(self):
        """Returns the next song, or None if there are no more.

        Will loop if 'loop' is enabled, and will shuffle if 'shuffle' is enabled.
        If there is more than one item and both options are enabled, next()
        will avoid re-playing the last played song."""

        if self.index+1 < len(self.songs):
            self.index += 1
            return self.songs[self.index]
        else:
            if not self.loop or not self.songs:
                return None

            # Shuffle songs if it set
            if self.shuffle and len(self.songs) > 1:
                current = self.songs[self.index]
                random.shuffle(self.songs)

                # Prevent double play a Song
                if self.songs[0] == current:
                    idx = random.randint(1, len(self.songs) - 1)
                    self.songs[0], self.songs[idx] = self.songs[idx], self.songs[0]

            self.index = -1
            return self.next()
