import asyncio
from youtube_dl import YoutubeDL
from youtube_dl.utils import DownloadError
from .song import Song


class YTLoader:
    """Retrieves Song Objects via youtube-dl"""

    async def loadSong(self, url: str):
        """Retrieves one Song Object via youtube-dl"""
        return await self.loadPlaylist(url, noplaylist=True)

    async def loadPlaylist(self, url: str, noplaylist=False):
        """Retrieves a List of Song Objects via youtube dl"""
        return await self.__loadFromUrl(url, noplaylist)

    async def __loadFromUrl(self, url: str, noplaylist):
        """Retrieves one or more songs for a url. If its a playlist, returns multiple Pairs.
        The results are (title, source) pairs
        """

        # Options for the youtube-dl
        ydlOpts = {
            'format': 'bestaudio[ext=webm]/best[ext=webm]',
            'youtube_include_dash_manifest': False,
            'noplaylist': noplaylist,
            'ignoreerrors': True,
            'nocheckcertificate': True,
            'logtostderr': False,
            'quiet': True,
            'postprocessors': [
                {'key': 'FFmpegExtractAudio',
                 'preferredcodec': 'webm',
                 'preferredquality': '192'},
                {'key': 'FFmpegMetadata'}
            ]
        }

        loop = asyncio.get_event_loop()
        return await loop.run_in_executor(None, self.__extractSongs, ydlOpts, url)

    @staticmethod
    def __extractSongs(ydlOpts, url: str):
        # Extract Info's from the URL
        with YoutubeDL(ydlOpts) as ydl:
            info = ydl.extract_info(url, download=False)

        # If no Info available -> theres something wrong
        if not info:
            raise DownloadError('Data could not be retrieved')

        # Get the Songs Data
        if '_type' in info and info['_type'] == 'playlist':
            entries = info['entries']
        else:
            entries = [info]

        # Return List of Song Objects
        return [Song(e['title'], url, e['url']) for e in entries]
