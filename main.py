import sys
import traceback
import os
import plugins
import logging
import configparser
import discord

from discord.ext import commands
from logging.handlers import TimedRotatingFileHandler
from configparser import SafeConfigParser

class EnvInterpolation(configparser.BasicInterpolation):
    """Interpolation which expands environment variables in values."""

    def before_get(self, parser, section, option, value, defaults):
        value = super().before_get(parser, section, option, value, defaults)
        return os.path.expandvars(value)


# Setup the Logging
def setup_logging(configLocal):
    # Deklaration
    logLvls = {
        'INFO': logging.INFO,
        'DEBUG': logging.DEBUG,
        'WARNING': logging.WARNING,
        'ERROR': logging.ERROR,
        'CRITICAL': logging.CRITICAL
    }
    formatter       = logging.Formatter('%(asctime)s %(levelname)s:%(name)s %(message)s')
    logLvlFile      = configLocal.get('DEFAULT', 'logLevelFile', fallback=logging.INFO)
    logLvlConsole   = configLocal.get('DEFAULT', 'logLevelConsole', fallback=logging.INFO)

    # Convert LogLevel
    if logLvlFile in logLvls:
        logLvlFile = logLvls[logLvlFile]
    else:
        logLvlFile = logging.INFO
    if logLvlConsole in logLvls:
        logLvlConsole = logLvls[logLvlConsole]
    else:
        logLvlConsole = logging.INFO

    # Create Log Directory
    os.makedirs('logs', exist_ok=True)

    # Create FileHandler
    handlerFile = logging.handlers.TimedRotatingFileHandler("logs/log.txt", when='midnight')
    handlerFile.setLevel(logLvlFile)
    handlerFile.setFormatter(formatter)

    # Create Handler for System Out logging
    handlerSysOut = logging.StreamHandler(sys.stdout)
    handlerSysOut.setFormatter(formatter)
    handlerSysOut.setLevel(logLvlConsole)

    # Set BasicConfig [Ignore Waring]
    logging.basicConfig(
        level=logLvlConsole,
        handlers=[
            handlerFile,
            handlerSysOut
        ])


# Global handler for bot command errors
async def command_error(ctx, exc):
    exc_type = type(exc)
    if exc_type in [commands.BadArgument, commands.MissingRequiredArgument]:
        await ctx.send("No wonder you're horrible at debates with arguments like that")
        return

    if ctx.message.guild is None or ctx.prefix.startswith(ctx.bot.user.mention):
        if exc_type is commands.CommandNotFound:
            await ctx.send(str(exc))
            return

    # from the original command error function
    print('Ignoring exception in command {}'.format(ctx.command), file=sys.stderr)
    traceback.print_exception(type(exc), exc, exc.__traceback__, file=sys.stderr)


# Program Start
if __name__ == '__main__':
    # Get Config
    #config = configparser.ConfigParser()
    #config = SafeConfigParser(os.environ)
    config = configparser.ConfigParser(interpolation=EnvInterpolation())
    config.read('config.ini')

    # Setup logging
    setup_logging(config)

    # Deklaration
    prefixes    = config.get('MUSICBOT', 'prefix', fallback='!')
    token       = config.get('MUSICBOT', 'token', fallback='')
    description = "Super's glorious and handsome bot"

    # Create Bot
    intent = discord.Intents(members=True)
    bot = commands.Bot(command_prefix=prefixes, description=description)
    bot.on_command_error = command_error
    bot.add_cog(plugins.MusicPlayerPlugin(bot, config))

    # Check Token if exists
    if token == '':
        logging.error('No Bot-Token defined in Configfile.')
    else:
        logging.info("SockenBot - Plugins initialized")
        bot.run(token)
        logging.info("SockenBot - Shut down")
