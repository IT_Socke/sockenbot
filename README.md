# SockenBot
## Important links
This project is based on the project of: https://github.com/CarlosFdez/SpueBox. But has been significantly reworked.

https://discord.com/developers/applications/

## Install docker on your pi
Followed by [this](sudo apt-get update && sudo apt-get upgra) instructions enter these commands on pi to install docker:
```
sudo apt-get update && sudo apt-get upgrade -y
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh
sudo usermod -aG docker pi
```
Now `logout` and login to check docker installation by command `docker version` without `sudo` command. You also need activate docker swarm by typing:
```
docker swarm init
```

## Check architecture
You can find out which architecure your machine uses by using the `arch` command. if it is armv7l (on every raspberry pi 3) you can leave the `docker-compose.yml` file like this. Otherwise another [image](https://gitlab.com/IT_Socke/ts3_bot/container_registry/) must be registered.

## run docker image
```
docker run -e CLIENTID=<CLIENTID> -e TOKEN=<TOKEN> registry.gitlab.com/it_socke/sockenbot
```
`CLIENTID` ID of your discord developer application  
`TOKEN` token of your discord bot   

## run with docker swarm
Copy the `docker-compose.yml` to your favorite dir an replace the docker image version on demand and the params `CLIENTID` and `TOKEN` in it. Afterwards you can deploy the docker stack with
```
docker stack deploy -c docker-compose.yml sockenbot
```